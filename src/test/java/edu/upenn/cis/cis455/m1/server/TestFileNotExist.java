package edu.upenn.cis.cis455.m1.server;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;
import edu.upenn.cis.cis455.TestHelper;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.handling.HttpIoHandler;
import edu.upenn.cis.cis455.m1.handling.HttpParsing;
import edu.upenn.cis.cis455.m1.handling.RequestHandler;
import edu.upenn.cis.cis455.m1.handling.ResponseHandler;

import org.apache.logging.log4j.Level;

public class TestFileNotExist {
    @Before
    public void setUp() {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
    }
    
    String sampleGetRequest = 
        "GET /a/b/hello.htm?q=x&v=12%200 HTTP/1.1\r\n";
    private Map<String, List<String>> parms = new HashMap<>();
    private Map<String, String> headers = new HashMap<>();
    @Test
    public void testFileNotExist() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Socket s = TestHelper.getMockSocket(
            sampleGetRequest, 
            byteArrayOutputStream);
        HttpTask task = new HttpTask(s, HttpTask.STEP.PARSE);
        RequestHandler req = new RequestHandler(headers, parms);
        ResponseHandler res = new ResponseHandler();
    	try {
			req.beginParse(task);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        try{
        	res.fetch(req);
        } catch (HaltException e) {
        	HttpIoHandler.sendException(s, null, e);
        }
        String result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
        System.out.println(result);
        assertTrue(result.startsWith("HTTP/1.1 404"));
    }

    
    @After
    public void tearDown() {}
}
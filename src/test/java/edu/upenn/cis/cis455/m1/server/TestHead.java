package edu.upenn.cis.cis455.m1.server;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;
import edu.upenn.cis.cis455.TestHelper;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.handling.HttpIoHandler;
import edu.upenn.cis.cis455.m1.handling.HttpParsing;
import edu.upenn.cis.cis455.m1.handling.RequestHandler;
import edu.upenn.cis.cis455.m1.handling.ResponseHandler;

import org.apache.logging.log4j.Level;

public class TestHead {
    @Before
    public void setUp() {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
    }
    
    String sampleGetRequest = 
        "HEAD /index.html HTTP/1.1\r\n";
    private Map<String, List<String>> parms = new HashMap<>();
    private Map<String, String> headers = new HashMap<>();
    @Test
    public void testHead() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Socket s = TestHelper.getMockSocket(
            sampleGetRequest, 
            byteArrayOutputStream);
        HttpTask task = new HttpTask(s, HttpTask.STEP.PARSE);
        RequestHandler req = new RequestHandler(headers, parms);
        ResponseHandler res = new ResponseHandler();
    	try {
			req.beginParse(task);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        try{
        	res.fetch(req);
        } catch (HaltException e) {
        	HttpIoHandler.sendException(s, null, e);
        }
        HttpIoHandler.sendResponse(s, req, res);
        Path pathInfo = Paths.get("www/index.html");
        int length = Files.readString(pathInfo).length();
        String type = Files.probeContentType(pathInfo);
        String pattern = "Content-Length:" + String.valueOf(length) + "\r\n" + "Content-Type:" + type + "\r\n";
        
        assertTrue(res.body().equals(pattern));
    }

    
    @After
    public void tearDown() {}
}
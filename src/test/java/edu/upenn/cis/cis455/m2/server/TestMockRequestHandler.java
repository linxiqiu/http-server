package edu.upenn.cis.cis455.m2.server;
import java.io.*;
import java.net.Socket;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.cis455.TestHelper;
import edu.upenn.cis.cis455.m1.handling.HttpIoHandler;
import edu.upenn.cis.cis455.m1.server.HttpTask;
import edu.upenn.cis.cis455.SparkController;

import org.apache.logging.log4j.Level;

public class TestMockRequestHandler {
    @Before
    public void setUp() {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
    }
    
    String sampleGetRequest = 
        "GET /testRoute HTTP/1.1";
    String expectedResult = "<html><head><title>Response</title></head><body><h1>Response</h1><p>Test</p></body></html>";
    
    @Test
    public void testMockRequestHandler() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        SparkController.get("/testRoute", new MockRequestHandler());
        
        Socket s = TestHelper.getMockSocket(
                sampleGetRequest, 
                byteArrayOutputStream);
        HttpTask task = new HttpTask(s, HttpTask.STEP.PARSE);
        HttpIoHandler handle = new HttpIoHandler();
        try {
        	handle.dispatch(task);
    	} catch (Exception e) {
    	// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
        String result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
        System.out.println(HttpIoHandler.res.body());
        assertTrue(HttpIoHandler.res.body().equals(expectedResult));
    }

    
    @After
    public void tearDown() {}
}
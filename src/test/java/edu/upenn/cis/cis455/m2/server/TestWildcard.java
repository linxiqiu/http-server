package edu.upenn.cis.cis455.m2.server;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;

import org.apache.logging.log4j.Level;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.cis455.SparkController;
import edu.upenn.cis.cis455.TestHelper;
import edu.upenn.cis.cis455.m1.handling.HttpIoHandler;
import edu.upenn.cis.cis455.m1.server.HttpTask;

public class TestWildcard {
	@Before
    public void setUp() {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
    }
    
    String sampleGetRequest = 
        "GET /testWildCard/I/Donuts HTTP/1.1";
    String expectedResult = "IloveDonuts";
    @Test
    public void testWildcard() throws IOException {
    		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            SparkController.get("/testWildCard/*/*", (req, res) -> {
            	return req.splat()[0] + "love" + req.splat()[1];
            });
            
            Socket s = TestHelper.getMockSocket(
                    sampleGetRequest, 
                    byteArrayOutputStream);
            HttpTask task = new HttpTask(s, HttpTask.STEP.PARSE);
            HttpIoHandler handle = new HttpIoHandler();
            try {
            	handle.dispatch(task);
        	} catch (Exception e) {
        	// TODO Auto-generated catch block
        		e.printStackTrace();
        	}
            
            assertTrue(HttpIoHandler.res.body().equals(expectedResult));
    }

    
    @After
    public void tearDown() {}
}

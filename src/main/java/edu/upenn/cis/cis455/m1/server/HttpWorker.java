package edu.upenn.cis.cis455.m1.server;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.m1.handling.HttpIoHandler;
/**
 * Stub class for a thread worker that handles Web requests
 */
public class HttpWorker implements Runnable {
	
	static final Logger logger = LogManager.getLogger(HttpWorker.class);
	
	private final 	HttpTaskQueue taskQueue;
	private boolean	isStopped   = false;
	
	public HttpWorker(HttpTaskQueue taskQueue) {
		this.taskQueue = taskQueue;
	}
	private HttpTask readFromQueue() throws InterruptedException {
		while (true) {
			synchronized (taskQueue) {
				if (taskQueue.isEmpty()) {
					logger.debug("Queue is currently empty");
					if (isStopped == true) {
						return null;
					}
					taskQueue.wait();
				}
				else {
					HttpTask task = taskQueue.readFromQueue();
					taskQueue.removeFromQueue();
					logger.debug("Notifying everyone we are removing an item");
					taskQueue.notifyAll();
					logger.debug("Exiting queue with return");
					return task;
				}
			}
		}
	}
    @Override
    public void run() {
        // TODO Auto-generated method stub
    	while (!isStopped) {
    		try {
    			logger.debug("Worker invoked.");
				HttpTask task = readFromQueue();
				if (!isStopped) {
					try {
						HttpIoHandler handler = new HttpIoHandler();
						handler.dispatch(task);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
				} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
					break;
			} catch (InterruptedException e) {
				logger.error("Interrupt Exception in Worker thread");
			}
    	}
    	logger.debug("Worker Stopped.");
    }
    public void stopWorker() {
    	this.isStopped = true;
    }
}

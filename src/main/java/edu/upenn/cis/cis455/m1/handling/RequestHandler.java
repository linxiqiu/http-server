package edu.upenn.cis.cis455.m1.handling;

import java.net.Socket;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


import edu.upenn.cis.cis455.m1.server.*;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m2.interfaces.Request;
import edu.upenn.cis.cis455.m2.interfaces.SessionImpl;
import edu.upenn.cis.cis455.m1.handling.HttpIoHandler;
import edu.upenn.cis.cis455.SparkController;

public class RequestHandler extends Request {
	public enum STEP   {PARSE, FETCH, RETURN};
	private String JSessionId;
	private List<String> splat = new ArrayList<String>();
    private Map<String, List<String>> parms;
    private Map<String, String> headers;
    private Map<String, String> params = new HashMap<String, String>();
    private Map<String, Object> attribute = new HashMap<String, Object>();
    
    public RequestHandler (Map<String, String> headers, Map<String, List<String>> parms) {
    	this.parms = parms;
    	this.headers = headers;
    }
    
	public void beginParse(HttpTask task) throws Exception {
		Socket socket = task.getSocket();
		try {
			String uri = HttpParsing.parseRequest(socket.getLocalAddress().toString(), socket.getInputStream(), headers, parms);
			Thread.currentThread().setName("ThreadPool" + uri);
		} catch(HaltException e) {
			throw e;
		}
      
	}
	@Override
	public String requestMethod() {
		// TODO Auto-generated method stub
		return headers.get("Method");
	}

	@Override
	public String host() {
		// TODO Auto-generated method stub
		return headers.get("host");
	}

	@Override
	public String userAgent() {
		// TODO Auto-generated method stub
		return headers.get("user-agent");
	}

	@Override
	public int port() {
		// TODO Auto-generated method stub
		String host = host();
		int p = host.indexOf(':');
		return Integer.parseInt(host.substring(p + 1).trim());
	}

	@Override
	public String pathInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String url() {
		// TODO Auto-generated method stub
		return headers.get("url");
	}

	@Override
	public String uri() {
		// TODO Auto-generated method stub
		return headers.get("uri");
	}

	@Override
	public String protocol() {
		// TODO Auto-generated method stub
		return headers.get("protocolVersion");
	}

	@Override
	public String contentType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String ip() {
		// TODO Auto-generated method stub
		return headers.get("http-client-ip");
	}

	@Override
	public String body() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int contentLength() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String headers(String name) {
		// TODO Auto-generated method stub
		return headers.get(name);
	}

	@Override
	public Set<String> headers() {
		// TODO Auto-generated method stub
		return headers.keySet();
	}
	@Override
	public SessionImpl session() {
		// TODO Auto-generated method stub
		return SparkController.getSession(JSessionId);
	}
	@Override
	public SessionImpl session(boolean create) {
		// TODO Auto-generated method stub
		if (create == true) {
			String id = SparkController.createSession();
			HttpIoHandler.res.cookie("JSESSIONID", id);
			return SparkController.getSession(id);
		}
		else
			return null;
	}
	@Override
	public Map<String, String> params() {
		// TODO Auto-generated method stub
		return params;
	}
	@Override
	public String queryParams(String param) {
		// TODO Auto-generated method stub
		return parms.get(param).get(0);
	}
	@Override
    /**
     * @return Get the list of values for the query parameter
     */
	public List<String> queryParamsValues(String param) {
		// TODO Auto-generated method stub
		return parms.get(param);
	}
	@Override
    /**
     * @return Get the list of Keys
     */
	public Set<String> queryParams() {
		// TODO Auto-generated method stub
		return parms.keySet();
	}
	@Override
	public String queryString() {
		// TODO Auto-generated method stub
		return headers.get("queryString");
	}
	@Override
	public void attribute(String attrib, Object val) {
		// TODO Auto-generated method stub
		attribute.put(attrib, val);
	}
	@Override
	public Object attribute(String attrib) {
		// TODO Auto-generated method stub
		return attribute.get(attrib);
	}
	@Override
	public Set<String> attributes() {
		// TODO Auto-generated method stub
		return attribute.keySet();
	}
	@Override
	public Map<String, String> cookies() {
		// TODO Auto-generated method stub
        String[] CookieStrings = headers.get("cookie").split("; ");
        for (String CookieString : CookieStrings) {
        	String[] valKey = CookieString.split("=");
        	if (valKey[0].equals("JSESSIONID")) {
        		this.JSessionId = valKey[1];
        	}
        	HttpIoHandler.res.cookie(valKey[0], valKey[1]);
        }
		return null;
	}
	
    /**
     * @return return the wildcard values
     */
	public String[] splat() {
		return splat.toArray(String[]::new);
	}
	public ArrayList<String> splatRaw() {
		return (ArrayList<String>) splat;
	}
	
}

package edu.upenn.cis.cis455.m1.server;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * Stub for your HTTP server, which listens on a ServerSocket and handles
 * requests
 */
public class HttpListener implements Runnable {
	private ServerSocket  serverSocket = null;
	private boolean		  isStopped	   = false;
	private Thread		  runningThread= null;
	private int			  serverPort   = WebService.port;
	
	private final HttpTaskQueue taskQueue;
	private final HttpWorkerPool workerPool;
	static final Logger logger = LogManager.getLogger(HttpTaskQueue.class);
	
	public HttpListener(HttpTaskQueue taskQueue, int threadNum) {
		this.taskQueue = taskQueue;
		workerPool = new HttpWorkerPool(this.taskQueue, WebService.threadPool());
	}
    @Override
    public void run() {
        // TODO Auto-generated method stub
    	//synchronized(this) {
    	//	this.runningThread = Thread.currentThread();
    	//}
    	openServerSocket();
    	while (!isStopped) {
    		try {
    			System.out.println("Listening for a connection");
    			if (isStopped == true)
    				break;
    			Socket socket = serverSocket.accept();
    			logger.debug("Socket Accpted");
    			workerPool.addToQueue(socket);
    		} catch (IOException e) {
    			if (isStopped()) {
    				System.out.println("Server Stopped.");
    				return;
    			}
    			throw new RuntimeException("Error accepting client connection", e);
    		}
    	}
    	System.out.println("Server Stopped.");
    	return;
    }
    	
    private boolean isStopped() {
		return this.isStopped;
	}
    public void stopServer() {
    	workerPool.stop();
    	this.isStopped = true;
    }
	private void openServerSocket() {
		try {
			logger.debug("Opened");
			this.serverSocket = new ServerSocket( serverPort );
		} catch (IOException e) {
			throw new RuntimeException("Cannot open port" + String.valueOf(WebService.port()), e);
		}
	}
}

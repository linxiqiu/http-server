package edu.upenn.cis.cis455.m1.server;

import java.net.Socket;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HttpWorkerPool {
	private final HttpTaskQueue taskQueue;
	private final ArrayList<HttpWorker> workerPool;
	static final Logger logger = LogManager.getLogger(HttpTaskQueue.class);
	
	public HttpWorkerPool(HttpTaskQueue taskQueue, int size) {
		this.taskQueue = taskQueue;
		workerPool = new ArrayList<HttpWorker>(size);
		for (int i = 0; i < size; i++) {
			workerPool.add(new HttpWorker(taskQueue));
		}
		int count = 1;
		for (HttpWorker i : workerPool) {
			Thread t = new Thread(i);
			t.setName("ThreadPool" + String.valueOf(count));
			t.start();
			count++;
		}
	}
	
    public void addToQueue(Socket s) {
    	logger.info("Adding element to queue");
    	while (true) {
    		synchronized (taskQueue) {
    			HttpTask newTask = new HttpTask(s, HttpTask.STEP.PARSE);
    			taskQueue.addToQueue(newTask);
    			taskQueue.notifyAll();
    			break;
    		}
    	}
    }
    public boolean stop() {
    	for (HttpWorker t : workerPool) {
    		t.stopWorker();
    	}
    	while (true) {
    		synchronized (taskQueue) {
    			taskQueue.notifyAll();
    			break;
    		}
    	}
    	return true;
    }
}

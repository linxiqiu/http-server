package edu.upenn.cis.cis455.m1.handling;

import java.io.*;
import java.util.Set;
import java.util.regex.Pattern;

public class ControlPanel {
	public static void getControlPanel(ResponseHandler res) {
		StringBuffer content = new StringBuffer();
		content.append("<!DOCTYPE html>\n");
		content.append("<html>\n");
		content.append("<head>\n");
		content.append("    <title>Control Panel</title>\n");
		content.append("</head>\n");
		content.append("<body>\n");
		
		content.append("<h2>ThreadPool &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Status or url</h2>\n");

		String pattern = "ThreadPool.*";
		Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
		for (Thread t : threadSet) {
		    String threadName = t.getName();
		    String[] name = threadName.split("/");
		    if (Pattern.matches(pattern, threadName)) {
		    	System.out.println(threadName);
		    	content.append("<h4>" + threadName + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

		    	if (name.length > 1 && t.getState().toString().equals("RUNNABLE")) {
			    	content.append("/" + name[1] + "</h4>\n");
			    }
			    else {
			    	content.append(t.getState().toString() + "</h4>\n");
			    }
		    }
		}
		content.append("<li><a href=\"/shutdown\">Shut down</a></li>");
		content.append("<h1>" + "Shit Things Happen!!!" + "</h1>\n");
		content.append("<h2>" + "Error Log File" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "</h2>\n");
		
		try{
			   FileInputStream fstream = new FileInputStream("app.log");
			   BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			   String strLine;
			   /* read log line by line */
			   while ((strLine = br.readLine()) != null)   {
			     /* parse strLine to obtain what you want */
			     if(strLine.contains("ERROR")) {
			    	 content.append("<h4>" + strLine + "</h4>\n");
			     }
			   }
			   fstream.close();
			} catch (Exception e) {
			     System.err.println("Error: " + e.getMessage());
			}
		content.append("</body>\n");
		content.append("</html>\n");
		res.bodyRaw(content.toString().getBytes());
	}
	
}

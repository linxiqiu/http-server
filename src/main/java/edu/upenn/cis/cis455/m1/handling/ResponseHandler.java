package edu.upenn.cis.cis455.m1.handling;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.*;
import java.util.*;
import java.util.regex.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.m2.interfaces.Cookie;
import edu.upenn.cis.cis455.m2.interfaces.Response;
import edu.upenn.cis.cis455.m2.server.WebService;
import edu.upenn.cis.cis455.SparkController;
import edu.upenn.cis.cis455.exceptions.HaltException;

public class ResponseHandler extends Response{
    protected int statusCode = 200;
    protected byte[] body;
    protected String contentType = "text/plain"; // e.g., "text/plain";
    final static Logger logger = LogManager.getLogger(ResponseHandler.class);
    private ArrayList<Cookie> cookieList = new ArrayList<Cookie> ();
    private Map<String, String> headers = new HashMap<String, String>();
    
    public int status() {
        return statusCode;
    }

    public void status(int statusCode) {
        this.statusCode = statusCode;
    }

    public String body() {
        try {
            return body == null ? "" : new String(body, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }
    }

    public byte[] bodyRaw() {
        return body;
    }

    public void bodyRaw(byte[] b) {
        body = b;
    }

    public void body(String body) {
        this.body = body == null ? null : body.getBytes();
    }

    public String type() {
        return contentType;
    }

    public void type(String contentType) {
        this.contentType = contentType;
    }
	public void fetch(RequestHandler req) throws IOException {
		System.out.println(req.uri());
		try {
			switch(req.requestMethod()) {
		case("GET"): {
			if (req.uri().equals("/control") ) {
				logger.debug("here");
				StringBuffer content = new StringBuffer();
				content.append("<!DOCTYPE html>\n");
				content.append("<html>\n");
				content.append("<head>\n");
				content.append("    <title>Control Panel</title>\n");
				content.append("</head>\n");
				content.append("<body>\n");
				
				content.append("<h2>ThreadPool &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Status or url</h2>\n");

				String pattern = "ThreadPool.*";
				Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
				for (Thread t : threadSet) {
				    String threadName = t.getName();
				    String[] name = threadName.split("/");
				    if (Pattern.matches(pattern, threadName)) {
				    	System.out.println(threadName);
				    	content.append("<h4>" + threadName + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

				    	if (name.length > 1 && t.getState().toString().equals("RUNNABLE")) {
					    	content.append("/" + name[1] + "</h4>\n");
					    }
					    else {
					    	content.append(t.getState().toString() + "</h4>\n");
					    }
				    }
				}
				content.append("<li><a href=\"/shutdown\">Shut down</a></li>");
				content.append("</body>\n");
				content.append("</html>");
				bodyRaw(content.toString().getBytes());
				return;
			}
			if (req.uri().equals("/shutdown")) {
				logger.debug("Shutting Down");
				SparkController.stop();
				Thread.currentThread().interrupt();
				return;
			}
			
			String filePath = WebService.staticFileLocation() + req.uri();
			System.out.println(req.pathInfo());
	        Path pathInfo = Paths.get(filePath);
	        if (Files.exists(pathInfo)) {
	            // file exist
	            String contentType = Files.probeContentType(pathInfo);
	            type(contentType);
	            bodyRaw(Files.readAllBytes(pathInfo));
	        } else {
	            // 404
	            throw new HaltException(404, "Not Found");
	        }
	        break;
		}
		case("HEAD"): {
			String filePath = "www" + req.uri();
	        Path pathInfo = Paths.get(filePath);
	        if (Files.exists(pathInfo)) {
	            // file exist
	            String contentType = Files.probeContentType(pathInfo);
	            type(contentType);
	            int length = Files.readAllBytes(pathInfo).length;
	            
	            String contentLength = "Content-Length:" + String.valueOf(length) + "\r\n";
	            
	            bodyRaw((contentLength + "Content-Type:" + type() + "\r\n").getBytes());
	        } else {
	            // 404
	        	throw new HaltException(404, "Not Found");
	        }
	        break;
		}
		}
		} catch (HaltException e) {
			throw e;
		}
	}
	@Override
	public String getHeaders() {
		// TODO Auto-generated method stub
		return headers.get("Location");
	}

	@Override
	public void header(String header, String value) {
		// TODO Auto-generated method stub
		this.headers.put(header, value);
	}

	@Override
	public void redirect(String location) {
		// TODO Auto-generated method stub
		redirect(location, 301);
	}

	@Override
	public void redirect(String location, int httpStatusCode) {
		// TODO Auto-generated method stub
		this.statusCode = 301;
		header("Location", location);
	}
	/*
	public void cookie(final char name, final char value) {
		cookie(String.valueOf(name), String.valueOf(value), -1, false);
	}
	*/
	@Override
	public void cookie(String name, String value) {
		// TODO Auto-generated method stub
        cookie(name, value, -1, false);
	}

	@Override
	public void cookie(String name, String value, int maxAge) {
		// TODO Auto-generated method stub
        cookie(name, value, maxAge, false);
	}

	@Override
	public void cookie(String name, String value, int maxAge, boolean secured) {
		// TODO Auto-generated method stub
        cookie(name, value, maxAge, secured, false);
	}

	@Override
	public void cookie(String name, String value, int maxAge, boolean secured, boolean httpOnly) {
		// TODO Auto-generated method stub
        cookie("", name, value, maxAge, secured, httpOnly);
	}

	@Override
	public void cookie(String path, String name, String value) {
		// TODO Auto-generated method stub
		cookie(path, name, value, -1);
	}

	@Override
	public void cookie(String path, String name, String value, int maxAge) {
		// TODO Auto-generated method stub
		cookie(path, name, value, maxAge, false);
	}

	@Override
	public void cookie(String path, String name, String value, int maxAge, boolean secured) {
		// TODO Auto-generated method stub
		cookie(path, name, value, maxAge, secured, false);
	}

	@Override
	public void cookie(String path, String name, String value, int maxAge, boolean secured, boolean httpOnly) {
		// TODO Auto-generated method stub
        Cookie cookie = new Cookie(name, value);
        cookie.setPath(path);
        cookie.setMaxAge(maxAge);
        cookie.setSecure(secured);
        cookie.setHttpOnly(httpOnly);
        cookieList.add(cookie);
	}
	
	@Override
	public void removeCookie(String name) {
		// TODO Auto-generated method stub
        removeCookie(null, name);
	}

	@Override
	public void removeCookie(String path, String name) {
		// TODO Auto-generated method stub
        Cookie cookie = new Cookie(name, "");
        cookie.setPath(path);
        cookie.setMaxAge(0);
        cookieList.add(cookie);
	}
	
	public ArrayList<Cookie> getCookie() {
		return cookieList;
	}
}

package edu.upenn.cis.cis455.m1.handling;

import java.net.Socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m2.interfaces.*;
import edu.upenn.cis.cis455.m1.server.*;
import edu.upenn.cis.cis455.m2.server.WebService;
import edu.upenn.cis.cis455.SparkController;

import java.io.OutputStream;
import java.io.IOException;

import java.util.*;
/**
 * Handles marshaling between HTTP Requests and Responses
 */
public class HttpIoHandler {
	public enum STEP   {PARSE, FETCH, RETURN};
	
    final static Logger logger = LogManager.getLogger(HttpIoHandler.class);
    private static Map<String, List<String>> parms = new HashMap<>();
    private static Map<String, String> headers = new HashMap<>();
    public static RequestHandler req = new RequestHandler(headers, parms);
    public static ResponseHandler res = new ResponseHandler(); 
    //private HttpTask task;
    //public HttpIoHandler(HttpTask task) {
    //	this.task = task;
    //}
    /**
     * Sends an exception back, in the form of an HTTP response code and message.
     * Returns true if we are supposed to keep the connection open (for persistent
     * connections).
     * @throws IOException 
     */
    public static boolean sendException(Socket socket, Request request, HaltException except) throws IOException {
    	
    	OutputStream clientOutput = socket.getOutputStream();
    	clientOutput.write(("HTTP/1.1 " + except.statusCode() + "\r\n").getBytes());
    	clientOutput.write(except.body().getBytes());
    	socket.close();
        return true;
    }

    /**
     * Sends data back. Returns true if we are supposed to keep the connection open
     * (for persistent connections).
     * @throws IOException 
     */
    public static boolean sendResponse(Socket socket, RequestHandler request, ResponseHandler response) throws IOException {
    	OutputStream clientOutput = socket.getOutputStream();
    	if (response.status() == 200) {
    		Date date = new Date();
            clientOutput.write((request.protocol()+ " " + response.status() + " OK" + "\r\n").getBytes());
            clientOutput.write(("Date:" + date.toString() + "\r\n").getBytes());
            clientOutput.write(("ContentType:" + response.type() + "\r\n").getBytes());
            clientOutput.write(("Content-Length: " + String.valueOf(response.bodyRaw().length) + "\r\n").getBytes());
            if (response.getCookie() != null) {
            	clientOutput.write((getCookieString(response.getCookie())).getBytes());
            }
            clientOutput.write("\r\n".getBytes());
            clientOutput.write(response.bodyRaw());
            clientOutput.write("\r\n\r\n".getBytes());
            clientOutput.flush();
            clientOutput.close();
        	socket.close();    		
    	}
    	else if (response.status() == 301) {
    		clientOutput.write((request.protocol()+ " " + response.status() + " Moved Permanently" + "\r\n").getBytes());
    		clientOutput.write((("Location: ") + response.getHeaders()).getBytes());
    		clientOutput.write("\r\n\r\n".getBytes());
    		clientOutput.flush();
            clientOutput.close();
        	socket.close();
    	}
        return true;
    }
    public void dispatch(HttpTask task) throws Exception {
    	switch(task.getStatus()) {
    	//Before part
    	
        case PARSE:
        {
        	try {
        		req.beginParse(task);
        	} catch (HaltException e) {
        		sendException(task.getSocket(), null, e);
    			return ;
        	}
        }
        case FETCH:
        {
        	try{
        		CommitFilter.Commit("BEFORE");
        	} catch (HaltException e) {
        		sendException(task.getSocket(), null, e);
        	}
        	
        	// Special URL
        	if (req.uri().equals("/control")) {
        		logger.error("Error!");
        		ControlPanel.getControlPanel(res);
        		sendResponse(task.getSocket(), req, res);
        		return ;
        	}
        	if (req.uri().equals("/shutdown")) {
				logger.debug("Shutting Down");
				task.getSocket().close();
				SparkController.stop();
				Thread.currentThread().interrupt();
				return;
        	}
        	
        	WebService serve = new WebService();
        	ArrayList<RouteImpl> routeList = SparkController.getRoute(req.requestMethod());
        	if (routeList == null) {
        		// changed
        		sendException(task.getSocket(), null, new HaltException(501, "Unimplemented method"));
    			return ;
        	}
        	boolean isMatched = false;
    		for (RouteImpl r : routeList) {
    			if(uriMatching(r.getPath(), req.uri())) {
    				isMatched = true;
    				try {
    					System.out.println("Begin fetching");
    					serve.render(req.requestMethod(), req.uri(), r);
    					break;
    				} catch (HaltException e) {
    					sendException(task.getSocket(), null, e);
    					return;
    				}
    			}
    		}
    		if (isMatched == false) {
    			logger.error("No Route Matched!");
    			sendException(task.getSocket(), null, new HaltException(404, "Not Found"));
    			return ;
    		}
        	task.changeStatus(HttpTask.STEP.RETURN);
        	try {
        		CommitFilter.Commit("AFTER");
        	} catch (HaltException e) {
        		sendException(task.getSocket(), null, e);
        		return ;
        	}
        	
        }
        case RETURN:
        {
        	logger.debug("Sending response");
        	sendResponse(task.getSocket(), req, res);
        }
        }
    	}
    
    public static boolean uriMatching(String path, String uri) {
    	// Named Parameters or Wildcard
    	System.out.println(path);
    	if (path.contains(":") || path.contains("*")) {
    		String[] pathKey = path.split("/");
    		String[] uriValue = uri.split("/");
    		int countAster = 0;
    		// Count how many wildcards in pathKey
    		for (String i : pathKey) {
    			if (i.equals("*"))
    				countAster++;
    		}
    		
    		for (int i = 0; i < pathKey.length; i++) {
    			if (pathKey[i].contains(":"))
    				req.params().put(pathKey[i], uriValue[i]);
    			else if (pathKey[i].equals("*")) {
    				countAster--;
    				req.splatRaw().add(uriValue[i]);
    				if (countAster == 0 || i == uriValue.length)
    					return true;
    			}
    			else if (i == pathKey.length || i == uriValue.length || !pathKey[i].equals(uriValue[i])) {
    				return false;
    			}
    		}
    	}
    	else
    		if (!path.equals(uri))
    			return false;
    	return true;
    }
    
    public static String getCookieString(ArrayList<Cookie> cookies) {
    	String cookieString = "";
    	for (Cookie f : cookies) {
    		if (f.getMaxAge() == -1) {
    			cookieString += "Set-Cookie: " + f.getName() + "=" + f.getValue() + "\r\n";
    		}
    		else {
    			cookieString += "Set-Cookie: " + f.getName()   + "=" + f.getValue() + "; " +
    							"Max-Age="     + f.getMaxAge() + "\r\n";
    		}
    			
    	}
		return cookieString;
    }
    }

package edu.upenn.cis.cis455.m1.server;

import java.util.LinkedList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Stub class for implementing the queue of HttpTasks
 */
public class HttpTaskQueue {
	static final Logger logger = LogManager.getLogger(HttpTaskQueue.class);
	
	private final LinkedList<HttpTask> taskQueue = new LinkedList<HttpTask>();
	
	public void addToQueue(HttpTask task) {
		taskQueue.add(task);
	}
	
	public HttpTask readFromQueue() throws InterruptedException {
		while (true) {
			if (taskQueue.isEmpty()) {
				logger.debug("Queue is currently empty.");
				taskQueue.wait();
			}
			else {
				HttpTask taskRightNow = taskQueue.peek();
				return taskRightNow;
			}
		}
	}
	public int getTaskNum() {
		return taskQueue.size();
	}
	public boolean isEmpty() {
		return taskQueue.isEmpty();
	}
	
	public void removeFromQueue() {
		taskQueue.remove(0);
	}
}

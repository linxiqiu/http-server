package edu.upenn.cis.cis455.m1.server;

import java.net.Socket;

public class HttpTask {
	public enum STEP   {PARSE, FETCH, RETURN};
	public	 	STEP   status;
    public		Socket requestSocket;

    public HttpTask(Socket socket, STEP s) {
    	this.status = s;
        this.requestSocket = socket;
    }

    public Socket getSocket() {
        return requestSocket;
    }
    
    public STEP getStatus() {
    	return this.status;
    }
    
    public void changeStatus(STEP s) {
    	this.status = s;
    }
}

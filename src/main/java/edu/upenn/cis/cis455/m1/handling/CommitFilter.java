package edu.upenn.cis.cis455.m1.handling;

import java.util.ArrayList;

import edu.upenn.cis.cis455.SparkController;
import edu.upenn.cis.cis455.m2.interfaces.FilterImpl;
import edu.upenn.cis.cis455.exceptions.HaltException;

public class CommitFilter {
	public static void Commit(String method){
		ArrayList<FilterImpl> beforeFilter = SparkController.getFilter("NULL" + method);

		if (beforeFilter != null) {
			for (FilterImpl f : beforeFilter)
				try {
					f.handle(HttpIoHandler.req, HttpIoHandler.res);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					throw (HaltException) e;
				}
		}
		else {
			beforeFilter = SparkController.getFilter(method);
			if (beforeFilter == null)
				return;
			for (FilterImpl f : beforeFilter) {
				System.out.println("in");
				if(HttpIoHandler.uriMatching(f.getPath(), HttpIoHandler.req.uri()))
					try {
						f.handle(HttpIoHandler.req, HttpIoHandler.res);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						throw (HaltException) e;
					}
			}
		}
	}
	
}

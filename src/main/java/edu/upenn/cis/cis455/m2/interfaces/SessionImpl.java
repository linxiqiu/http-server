package edu.upenn.cis.cis455.m2.interfaces;

import java.util.*;

public class SessionImpl extends Session{
	private Map<String, Object> attribute = new HashMap<String, Object>();
	private int interval;
	private long creationTime = System.currentTimeMillis();
	private String id = UUID.randomUUID().toString();
	private long lastAccessTime;
	private boolean validate;
	
	public SessionImpl() {
		this(600);
	}
	public SessionImpl(int interval) {
		this.interval = interval;
		this.validate = true;
	}
	@Override
	public String id() {
		// TODO Auto-generated method stub
		return this.id;
	}

	@Override
	public long creationTime() {
		// TODO Auto-generated method stub
		return this.creationTime;
	}

	@Override
	public long lastAccessedTime() {
		// TODO Auto-generated method stub
		return this.lastAccessTime;
	}

	@Override
	public void invalidate() {
		// TODO Auto-generated method stub
		this.validate = false;
	}
	
	public boolean isValidate() {
		return this.validate;
	}

	@Override
	public int maxInactiveInterval() {
		// TODO Auto-generated method stub
		return this.interval;
	}

	@Override
	public void maxInactiveInterval(int interval) {
		// TODO Auto-generated method stub
		this.interval = interval;
	}

	@Override
	public void access() {
		// TODO Auto-generated method stub
		this.lastAccessTime = System.currentTimeMillis();
		if ((lastAccessTime - creationTime) / 1000 > interval) {
			this.invalidate();
		}
	}

	@Override
	public void attribute(String name, Object value) {
		// TODO Auto-generated method stub
		attribute.put(name, value);
	}

	@Override
	public Object attribute(String name) {
		// TODO Auto-generated method stub
		return attribute.get(name);
	}

	@Override
	public Set<String> attributes() {
		// TODO Auto-generated method stub
		return attribute.keySet();
	}

	@Override
	public void removeAttribute(String name) {
		// TODO Auto-generated method stub
		attribute.remove(name);
	}
	public void setInterval(int interval) {
		this.interval = interval;
	}
}

package edu.upenn.cis.cis455.m2.interfaces;

import edu.upenn.cis.cis455.m1.handling.RequestHandler;
import edu.upenn.cis.cis455.m1.handling.ResponseHandler;


public class RouteImpl implements Route {
	private String path;
	private Route route;
	
	public RouteImpl(String path, Route route) {
		this.path = path;
		this.route = route;
	}
	@Override
	public Object handle(RequestHandler request, ResponseHandler response) throws Exception {
		// TODO Auto-generated method stub
		return route.handle(request, response);
	}
	public String getPath() {
		return path;
	}
}

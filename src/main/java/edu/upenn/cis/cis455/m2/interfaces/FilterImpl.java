package edu.upenn.cis.cis455.m2.interfaces;
import edu.upenn.cis.cis455.m1.handling.RequestHandler;
import edu.upenn.cis.cis455.m1.handling.ResponseHandler;

public class FilterImpl implements Filter {
	private String path;
	private Filter filter;
	public FilterImpl(Filter filter) {
		this.filter = filter;
	}
	public FilterImpl(String path, Filter filter) {
		this(filter);
		this.path = path;
	}
	@Override
	public void handle(RequestHandler request, ResponseHandler response) throws Exception {
		// TODO Auto-generated method stub
		filter.handle(request, response);
	}
	public String getPath() {
		return this.path;
	}
}

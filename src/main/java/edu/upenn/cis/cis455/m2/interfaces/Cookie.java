package edu.upenn.cis.cis455.m2.interfaces;

public class Cookie {
	private String name;
	private String value;
	private String path;
	private boolean isHttpOnly;
	private int maxAge;
	private boolean secured;
	
	public Cookie(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}

	public void setSecure(boolean secured) {
		this.secured = secured;
	}

	public void setHttpOnly(boolean httpOnly) {
		this.isHttpOnly = httpOnly;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public String getPath() {
		return this.path;
	}
	
	public int getMaxAge() {
		return this.maxAge;
	}
}

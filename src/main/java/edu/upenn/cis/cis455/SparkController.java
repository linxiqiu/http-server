/**
 * CIS 455/555 route-based HTTP framework.
 * 
 * Basic service handler and route manager.
 * 
 * V. Liu, Z. Ives
 * 
 * Portions excerpted from or inspired by Spark Framework, 
 * 
 *                 http://sparkjava.com,
 * 
 * with license notice included below.
 */

/*
 * Copyright 2011- Per Wendel
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.cis455;

import java.util.ArrayList;
import java.util.HashMap;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m2.interfaces.Route;
import edu.upenn.cis.cis455.m2.interfaces.RouteImpl;
import edu.upenn.cis.cis455.m2.interfaces.Filter;
import edu.upenn.cis.cis455.m2.interfaces.FilterImpl;
import edu.upenn.cis.cis455.m2.interfaces.SessionImpl;

// change to to edu.upenn.cis.cis455.m2 for m2
import edu.upenn.cis.cis455.m2.server.WebService;

public class SparkController {
	
	private static WebService server = new WebService();
	static private HashMap<String, ArrayList<FilterImpl>> filterMap = new HashMap<String, ArrayList<FilterImpl>>();
	static private HashMap<String, ArrayList<RouteImpl>> routingMap = new HashMap<String, ArrayList<RouteImpl>>();
	static private HashMap<String, SessionImpl> sessionMap = new HashMap<String, SessionImpl>();
    // We don't want people to use the constructor
    protected SparkController() {}

    /**
     * Passing the route 
     */
	public static ArrayList<RouteImpl> getRoute(String method) {
		return routingMap.get(method);
	}
	
	/**
     * Passing the filter 
     */
	public static ArrayList<FilterImpl> getFilter(String method) {
		return filterMap.get(method);
	}
	
    /**
     * Milestone 2 only: Handle an HTTP GET request to the path
     * This is actually registering.
     * @throws Exception 
     */
    public static void get(String path, Route route) {
    	RouteImpl routeNew = new RouteImpl(path, route);
		if (routingMap.containsKey("GET")) {
			// The method already exists
			routingMap.get("GET").add(routeNew);
		}
		else {
			// The method does not exist, so register a new one
			routingMap.put("GET", new ArrayList<RouteImpl>());
			routingMap.get("GET").add(routeNew);
		}
        //throw new UnsupportedOperationException();
    }

    /**
     * Milestone 2 only: Handle an HTTP POST request to the path
     */
    public static void post(String path, Route route) {
    	RouteImpl routeNew = new RouteImpl(path, route);
		if (routingMap.containsKey("POST")) {
			// The method already exists
			routingMap.get("POST").add(routeNew);
		}
		else {
			// The method does not exist, so register a new one
			routingMap.put("POST", new ArrayList<RouteImpl>());
			routingMap.get("POST").add(routeNew);
		}
        //throw new UnsupportedOperationException();
    }

    /**
     * Milestone 2 only: Handle an HTTP PUT request to the path
     */
    public static void put(String path, Route route) {
    	RouteImpl routeNew = new RouteImpl(path, route);
		if (routingMap.containsKey("PUT")) {
			// The method already exists
			routingMap.get("PUT").add(routeNew);
		}
		else {
			// The method does not exist, so register a new one
			routingMap.put("PUT", new ArrayList<RouteImpl>());
			routingMap.get("PUT").add(routeNew);
		}
        //throw new UnsupportedOperationException();
    }

    /**
     * Milestone 2 only: Handle an HTTP DELETE request to the path
     */
    public static void delete(String path, Route route) {
    	RouteImpl routeNew = new RouteImpl(path, route);
		if (routingMap.containsKey("DELETE")) {
			// The method already exists
			routingMap.get("DELETE").add(routeNew);
		}
		else {
			// The method does not exist, so register a new one
			routingMap.put("DELETE", new ArrayList<RouteImpl>());
			routingMap.get("DELETE").add(routeNew);
		}
        //throw new UnsupportedOperationException();
    }

    /**
     * Milestone 2 only: Handle an HTTP HEAD request to the path
     */
    public static void head(String path, Route route) {
    	RouteImpl routeNew = new RouteImpl(path, route);
		if (routingMap.containsKey("HEAD")) {
			// The method already exists
			routingMap.get("HEAD").add(routeNew);
		}
		else {
			// The method does not exist, so register a new one
			routingMap.put("HEAD", new ArrayList<RouteImpl>());
			routingMap.get("HEAD").add(routeNew);
		}
        //throw new UnsupportedOperationException();
    }

    /**
     * Milestone 2 only: Handle an HTTP OPTIONS request to the path
     */
    public static void options(String path, Route route) {
    	RouteImpl routeNew = new RouteImpl(path, route);
		if (routingMap.containsKey("OPTIONS")) {
			// The method already exists
			routingMap.get("OPTIONS").add(routeNew);
		}
		else {
			// The method does not exist, so register a new one
			routingMap.put("OPTIONS", new ArrayList<RouteImpl>());
			routingMap.get("OPTIONS").add(routeNew);
		}
        //throw new UnsupportedOperationException();
    }

    ///////////////////////////////////////////////////
    // HTTP request filtering
    ///////////////////////////////////////////////////

    /**
     * Milestone 2 only: Add filters that get called before a request
     */
    public static void before(Filter... filters) {
    	for (Filter filter : filters) {
    		FilterImpl filterNew = new FilterImpl(filter);
    		if (filterMap.containsKey("NULLBEFORE")) {
    			// The method already exists
    			filterMap.get("NULLBEFORE").add(filterNew);
    		}
    		else {
    			// The method does not exist, so register a new one
    			filterMap.put("NULLBEFORE", new ArrayList<FilterImpl>());
    			filterMap.get("NULLBEFORE").add(filterNew);
    		}
    	}
    	
        //throw new UnsupportedOperationException();
    }

    /**
     * Milestone 2 only: Add filters that get called after a request
     */
    public static void after(Filter... filters) {
    	for (Filter filter : filters) {
    		FilterImpl filterNew = new FilterImpl(filter);
    		if (filterMap.containsKey("NULLAFTER")) {
    			// The method already exists
    			filterMap.get("NULLAFTER").add(filterNew);
    		}
    		else {
    			// The method does not exist, so register a new one
    			filterMap.put("NULLAFTER", new ArrayList<FilterImpl>());
    			filterMap.get("NULLAFTER").add(filterNew);
    		}
    	}
        //throw new UnsupportedOperationException();
    }

    /**
     * Milestone 2 only: Add filters that get called before a request
     */
    public static void before(String path, Filter... filters) {
    	for (Filter filter : filters) {
    		FilterImpl filterNew = new FilterImpl(path, filter);
    		if (filterMap.containsKey(path)) {
    			// The method already exists
    			filterMap.get("BEFORE").add(filterNew);
    		}
    		else {
    			// The method does not exist, so register a new one
    			filterMap.put("BEFORE", new ArrayList<FilterImpl>());
    			filterMap.get("BEFORE").add(filterNew);
    		}
    	}
        //throw new UnsupportedOperationException();
    }

    /**
     * Milestone 2 only: Add filters that get called after a request
     */
    public static void after(String path, Filter... filters) {
    	for (Filter filter : filters) {
    		FilterImpl filterNew = new FilterImpl(path, filter);
    		if (filterMap.containsKey(path)) {
    			// The method already exists
    			filterMap.get("AFTER").add(filterNew);
    		}
    		else {
    			// The method does not exist, so register a new one
    			filterMap.put("AFTER", new ArrayList<FilterImpl>());
    			filterMap.get("AFTER").add(filterNew);
    		}
    	}
        //throw new UnsupportedOperationException();
    }
    
    
    // The following 2 functions are OPTIONAL for Milestone 1.
    // They will be used in Milestone 2 so user code can fail
    // and we will gracefully return something.

    /**
     * Triggers a HaltException that terminates the request
     */
    public static HaltException halt() {
        throw new UnsupportedOperationException();
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public static HaltException halt(int statusCode, String body) {
        throw new HaltException(statusCode, body);
    }

    ////////////////////////////////////////////
    // Server configuration
    ////////////////////////////////////////////

    /**
     * Set the IP address to listen on (default 0.0.0.0)
     */
    public static void ipAddress(String ipAddress) {
    	server.ipAddress(ipAddress);
        //throw new UnsupportedOperationException();
    }

    /**
     * Set the port to listen on (default 45555)
     */
    public static void port(int port) {
    	server.port(port);
        //throw new UnsupportedOperationException();
    }

    /**
     * Set the size of the thread pool
     */
    public static void threadPool(int threads) {
    	server.threadPool(threads);
        //throw new UnsupportedOperationException();
    }

    /**
     * Set the root directory of the "static web" files
     */
    public static void staticFileLocation(String directory) {
    	server.staticFileLocation(directory);
        //throw new UnsupportedOperationException();
    }

    /**
     * Hold until the server is fully initialized
     */
    public static void awaitInitialization() {
    	try {
			server.awaitInitialization();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        //throw new UnsupportedOperationException();
    }

    /**
     * Gracefully shut down the server
     */
    public static void stop() {
    	server.stop();
        //throw new UnsupportedOperationException();
    }
    public static String createSession() {
        //throw new UnsupportedOperationException();
    	SessionImpl session = new SessionImpl();
    	sessionMap.put(session.id(), session);
    	return session.id();
    }

    public static SessionImpl getSession(String id) {
        //throw new UnsupportedOperationException();
    	SessionImpl session = sessionMap.get(id);
    	session.access();
    	if(session.isValidate()) {
    		return session;
    	}
    	else
    		return null;
    }
}
